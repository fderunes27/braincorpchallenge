##Passwd as a Service

###Tomcat
 This has been tested using the tomcat version: apache-tomcat-8.5.28   
 In order to run and test the service, modify the catalina.properties file with the path to the passwd file and the groups file
 
    braincorp.challenge.passwd.path=<absolute path to the passwd file> e.g. D:/passwd or /etc/passwd
    braincorp.challenge.group.path=<absolute path to the groups file> e.g. D:/group or /etc/groups
http://localhost:8190/challenge/swagger/ is the swagger URL once the service is running on your local environment

###IDE
This service was implemented using Intellij 2017.3.4 (Community Edition) IDE

Configuration of the Tomcat Runner plugin
    
    Run/Debug Configurations    
    Tomcat Runner    
    >Tomcat installation: <path to the Tomcat Directory>
    >Before Launch configuration
    1. Run Maven Goal: clean install -DskipTests
    2. External Tool: 
     Program: cp
     Arguments: target/challenge.war <path to the Tomcat Directory>/webapps 

###Testing
The service is unit tested
The service can be accessed via the Swagger Url listed above for manual testing 