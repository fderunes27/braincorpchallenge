package com.braincorp.challenge.service;

import com.braincorp.challenge.controller.GroupController;
import com.braincorp.challenge.controller.PasswdController;
import com.braincorp.challenge.model.Error;
import com.braincorp.challenge.model.Group;
import com.braincorp.challenge.model.User;
import com.braincorp.challenge.util.IO;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.unitils.reflectionassert.ReflectionComparatorMode.LENIENT_ORDER;

@PrepareForTest(PasswdService.class)
public class PasswdServiceTest {

    @InjectMocks
    private PasswdService passwdService;

    @Mock
    private PasswdController passwdControllerMock;

    @Mock
    private GroupController groupControllerMock;

    @Mock
    private List<Error> serviceErrorMock;

    @BeforeTest
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    // Tests GetUsers
    @Test
    public void testGetUsers() {
        List<User> expectedUsers = Arrays.asList(IO.createFromResource("test/passwd/passwd2byquery.json", User[].class));
        PowerMockito.doReturn(expectedUsers).when(passwdControllerMock).getUsers();
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getUsers();

        Assert.assertEquals(response.getStatus(), 200);
        ReflectionAssert.assertReflectionEquals(expectedUsers, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetUsersServiceError() {
        List<Error> serviceErrors = new ArrayList<>();
        serviceErrors.add(new Error(500, "blah"));
        serviceErrors.add(new Error(501, "blah2"));

        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, serviceErrors);
        Response response = passwdService.getUsers();

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetUsersMalformPasswdfileError() {
        List<Error> serviceErrors = new ArrayList<>();
        serviceErrors.add(new Error(500, "Malform passwd file"));
        PowerMockito.doThrow(new RuntimeException(serviceErrors.get(0).getDescription())).when(passwdControllerMock).getUsers();
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getUsers();

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    // Tests GetUserByUid
    @Test
    public void testGetUserByUid() {
        User expectedUsers = IO.createFromResource("test/passwd/userbyuid.json", User.class);
        PowerMockito.doReturn(expectedUsers).when(passwdControllerMock).getUserByUid(Mockito.anyInt());
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getUserByUid(0);

        Assert.assertEquals(response.getStatus(), 200);
        ReflectionAssert.assertReflectionEquals(expectedUsers, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetUserByUidServiceError() {
        List<Error> serviceErrors = new ArrayList<>();
        serviceErrors.add(new Error(500, "blah"));
        serviceErrors.add(new Error(501, "blah2"));

        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, serviceErrors);
        Response response = passwdService.getUserByUid(0);

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetUserByUidMalformPasswdfileError() {
        List<Error> serviceErrors = new ArrayList<>();
        serviceErrors.add(new Error(500, "Malform passwd file"));
        PowerMockito.doThrow(new RuntimeException(serviceErrors.get(0).getDescription())).when(passwdControllerMock).getUserByUid(Mockito.anyInt());
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getUserByUid(0);

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetUserByUidNotFound() {
        List<Error> serviceErrors = new ArrayList<>();
        int uid = 1;
        serviceErrors.add(new Error(404, "No user information found for the uid " + uid));
        PowerMockito.doReturn(null).when(passwdControllerMock).getUserByUid(uid);
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getUserByUid(uid);
        Assert.assertEquals(response.getStatus(), 404);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    // Tests GetUsersQuery
    @Test
    public void testGetUsersQuery() throws IllegalAccessException {
        List<User> expectedUsers = Arrays.asList(IO.createFromResource("test/passwd/passwd2byquery.json", User[].class));
        PowerMockito.doReturn(expectedUsers).when(passwdControllerMock).getUsers(Mockito.any());
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getUsersByQuery("testName", 0, 0, "", "", "");

        Assert.assertEquals(response.getStatus(), 200);
        ReflectionAssert.assertReflectionEquals(expectedUsers, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetUsersQueryServiceError() {
        List<Error> serviceErrors = new ArrayList<>();
        serviceErrors.add(new Error(500, "blah"));
        serviceErrors.add(new Error(501, "blah2"));

        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, serviceErrors);
        Response response = passwdService.getUsersByQuery("testName", 0, 0, "", "", "");

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetUsersQueryMalformPasswdfileError() throws IllegalAccessException {
        List<Error> serviceErrors = new ArrayList<>();
        serviceErrors.add(new Error(500, "Malform passwd file"));
        PowerMockito.doThrow(new RuntimeException(serviceErrors.get(0).getDescription())).when(passwdControllerMock).getUsers(Mockito.any());
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getUsersByQuery("testName", 0, 0, "", "", "");

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    // Test GetGroup
    @Test
    public void testGetGroups() {
        List<Group> expectedGroups = Arrays.asList(IO.createFromResource("test/group/group3.json", Group[].class));
        PowerMockito.doReturn(expectedGroups).when(groupControllerMock).getGroups();
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getGroups();

        Assert.assertEquals(response.getStatus(), 200);
        ReflectionAssert.assertReflectionEquals(expectedGroups, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetGroupsServiceError() {
        List<Error> serviceErrors = new ArrayList<>();
        serviceErrors.add(new Error(500, "blah"));
        serviceErrors.add(new Error(501, "blah2"));

        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, serviceErrors);
        Response response = passwdService.getGroups();

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetGroupsMalformGroupfileError() {
        List<Error> serviceErrors = new ArrayList<>();
        serviceErrors.add(new Error(500, "Malform group file"));
        PowerMockito.doThrow(new RuntimeException(serviceErrors.get(0).getDescription())).when(groupControllerMock).getGroups();
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getGroups();

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    // Test GetGroupByGid
    @Test
    public void testGetGroupByGid() {
        Group expectedGroup = IO.createFromResource("test/group/group.json", Group.class);
        PowerMockito.doReturn(expectedGroup).when(groupControllerMock).getGroupByGid(expectedGroup.getGid());
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getGroupsByGid(expectedGroup.getGid());

        Assert.assertEquals(response.getStatus(), 200);
        ReflectionAssert.assertReflectionEquals(expectedGroup, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetGroupByGidServiceError() {
        List<Error> serviceErrors = new ArrayList<>();
        serviceErrors.add(new Error(500, "blah"));
        serviceErrors.add(new Error(501, "blah2"));

        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, serviceErrors);
        Response response = passwdService.getGroupsByGid(0);

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetGroupByGidMalformGroupfileError() {
        List<Error> serviceErrors = new ArrayList<>();
        serviceErrors.add(new Error(500, "Malform group file"));
        PowerMockito.doThrow(new RuntimeException(serviceErrors.get(0).getDescription())).when(groupControllerMock).getGroupByGid(Mockito.anyInt());
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getGroupsByGid(0);

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetGroupByUidNotFound() {
        List<Error> serviceErrors = new ArrayList<>();
        int gid = 1;
        serviceErrors.add(new Error(404, "No group information found for the gid " + gid));
        PowerMockito.doReturn(null).when(groupControllerMock).getGroupByGid(gid);
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getGroupsByGid(gid);
        Assert.assertEquals(response.getStatus(), 404);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    // Test GetGroupsByUid
    @Test
    public void testGetGroupsByUid() {
        User expectedUser = IO.createFromResource("test/passwd/usergroup.json", User.class);
        List<Group> expectedGroups = Arrays.asList(IO.createFromResource("test/group/group2.json", Group[].class));

        PowerMockito.doReturn(expectedUser).when(passwdControllerMock).getUserByUid(expectedUser.getUid());
        PowerMockito.doReturn(expectedGroups).when(groupControllerMock).getGroupsByUsername(expectedUser.getName());

        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getGroupsByUid(expectedUser.getUid());

        Assert.assertEquals(response.getStatus(), 200);
        ReflectionAssert.assertReflectionEquals(expectedGroups, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetGroupsByUidServiceError() {
        List<Error> serviceErrors = new ArrayList<>();
        serviceErrors.add(new Error(500, "blah"));
        serviceErrors.add(new Error(501, "blah2"));

        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, serviceErrors);
        Response response = passwdService.getGroupsByUid(0);

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetGroupsByUidMalformpasswdfilesError() {
        List<Error> serviceErrors = new ArrayList<>();
        
        serviceErrors.add(new Error(500, "Malform passwd file"));

        PowerMockito.doThrow(new RuntimeException(serviceErrors.get(0).getDescription())).when(passwdControllerMock).getUserByUid(Mockito.anyInt());
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getGroupsByUid(0);

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetGroupsByUidMalformgroupfilesError() {
        List<Error> serviceErrors = new ArrayList<>();
        serviceErrors.add(new Error(500, "Malform group file"));

        User expectedUser = IO.createFromResource("test/passwd/userbyuid.json", User.class);
        PowerMockito.doReturn(expectedUser).when(passwdControllerMock).getUserByUid(expectedUser.getUid());

//        PowerMockito.doReturn(new RuntimeException(serviceErrors.get(0).getDescription())).when(passwdControllerMock).getUserByUid(Mockito.anyInt());
        PowerMockito.doThrow(new RuntimeException(serviceErrors.get(0).getDescription())).when(groupControllerMock).getGroupsByUsername(expectedUser.getName());
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getGroupsByUid(expectedUser.getUid());

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    // Test GetGroupQuery
    @Test
    public void testGetGroupsQuery() throws IllegalAccessException {
        List<Group> expectedGroups = Arrays.asList(IO.createFromResource("test/group/group3.json", Group[].class));
        PowerMockito.doReturn(expectedGroups).when(groupControllerMock).getGroups(Mockito.any());
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getGroupsByQuery("testName", 0, new ArrayList<>());

        Assert.assertEquals(response.getStatus(), 200);
        ReflectionAssert.assertReflectionEquals(expectedGroups, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetGroupsQueryServiceError() {
        List<Error> serviceErrors = new ArrayList<>();
        serviceErrors.add(new Error(500, "blah"));
        serviceErrors.add(new Error(501, "blah2"));

        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, serviceErrors);
        Response response = passwdService.getGroupsByQuery("testName", 0, new ArrayList<>());

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testGetGroupsQueryMalformGroupfileError() throws IllegalAccessException {
        List<Error> serviceErrors = new ArrayList<>();
        serviceErrors.add(new Error(500, "Malform group file"));
        PowerMockito.doThrow(new RuntimeException(serviceErrors.get(0).getDescription())).when(groupControllerMock).getGroups(Mockito.any());
        PasswdService passwdService = new PasswdService(passwdControllerMock, groupControllerMock, new ArrayList<>());

        Response response = passwdService.getGroupsByQuery("testName", 0, new ArrayList<>());

        Assert.assertEquals(response.getStatus(), 500);
        ReflectionAssert.assertReflectionEquals(serviceErrors, response.getEntity(), LENIENT_ORDER);
    }

    @Test
    public void testDefaultController() {
        List<Error> expectedServiceErrors = new ArrayList<>();
        expectedServiceErrors.add(new Error(500, "Unable to read passwd file"));
        expectedServiceErrors.add(new Error(500, "Unable to read group file"));

        System.setProperty("braincorp.challenge.passwd.path","blah");
        System.setProperty("braincorp.challenge.group.path","blah");

        ReflectionAssert.assertReflectionEquals(new PasswdService().getServiceErrors(),expectedServiceErrors, LENIENT_ORDER);

    }

}
