package com.braincorp.challenge.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Scanner;

/**
 * This is a util class responsible to read data on the disk. It is here
 * temporary until it gets moved into a util package common to all the services.
 *
 * @author fderunes
 */
public class IO {
    private static Logger LOG = LogManager.getLogger(IO.class);

    /**
     * Opens, reads and closes a resource from the classpath.
     *
     * @param path The path to a resource in the classpath
     * @return The contents of the resource
     */
    public static synchronized String readClasspathResourceAsString(String path) {
        /*
         * Always assume path is an absolute path.
         */

        if (!path.startsWith("/")) {
            path = "/" + path;
        }

        // Using a try / catch so Scanner will close no matter what
        try (Scanner scanner = new Scanner(IO.class.getResourceAsStream(path), "UTF-8")) {
            return scanner.useDelimiter("\\A").next();
        } catch (Exception ex) {
            String err = "ERROR unable to read resource: " + path;
            throw new RuntimeException(err, ex);
        }
    }

    /**
     * Creates a Bean from a Json file
     *
     * @param path The path to a resource in the classpath
     * @param type The type of the json resource
     * @return
     */
    public static <T> T createFromResource(String path, Class<T> type) {
        try {
            String json = IO.readClasspathResourceAsString(path);

            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            T bean;

            bean = mapper.readValue(
                    json,
                    type);
            return bean;
        } catch (IOException e) {
            LOG.error("create from resource failed", e);
        }
        throw new RuntimeException("createFromResource failed");

    }

}
