package com.braincorp.challenge.controller;

import com.braincorp.challenge.model.User;
import com.braincorp.challenge.util.IO;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import java.util.Arrays;
import java.util.List;

import static org.unitils.reflectionassert.ReflectionComparatorMode.LENIENT_ORDER;

public class PasswdControllerTest {
    @Test
    public void testGetUsers() {
        User[] expectedUsers = IO.createFromResource("test/passwd/passwd3.json", User[].class);
        PasswdController passwdController = new PasswdController(IO.readClasspathResourceAsString("test/passwd/passwd3"));
        List<User> actualResult = passwdController.getUsers();
        ReflectionAssert.assertReflectionEquals(Arrays.asList(expectedUsers), actualResult, LENIENT_ORDER);
    }

    @Test
    public void testGetQueryUsersByUidAndComment() throws IllegalAccessException {
        User[] expectedUsers = IO.createFromResource("test/passwd/passwd2byquery.json", User[].class);

        PasswdController passwdController = new PasswdController(IO.readClasspathResourceAsString("test/passwd/passwd3"));

        User userFilter = IO.createFromResource("test/passwd/userfilterbyuidcomment.json", User.class);
        List<User> actualResult = passwdController.getUsers(userFilter);
        ReflectionAssert.assertReflectionEquals(Arrays.asList(expectedUsers), actualResult, LENIENT_ORDER);
    }

    @Test
    public void testGetQueryUsersByName() throws IllegalAccessException {
        User[] expectedUsers = IO.createFromResource("test/passwd/passwd1byquery.json", User[].class);

        PasswdController passwdController = new PasswdController(IO.readClasspathResourceAsString("test/passwd/passwd3"));

        User userFilter = IO.createFromResource("test/passwd/userfilterbyname.json", User.class);
        List<User> actualResult = passwdController.getUsers(userFilter);
        ReflectionAssert.assertReflectionEquals(Arrays.asList(expectedUsers), actualResult, LENIENT_ORDER);
    }

    @Test
    public void testGetQueryUsersNoParam() throws IllegalAccessException {
        User[] expectedUsers = IO.createFromResource("test/passwd/passwd3.json", User[].class);

        PasswdController passwdController = new PasswdController(IO.readClasspathResourceAsString("test/passwd/passwd3"));

        User userFilter = IO.createFromResource("test/passwd/userNull.json", User.class);
        List<User> actualResult = passwdController.getUsers(userFilter);
        ReflectionAssert.assertReflectionEquals(Arrays.asList(expectedUsers), actualResult, LENIENT_ORDER);
    }

    @Test
    public void testGetQueryUsersEmptyResult() throws IllegalAccessException {
        User[] expectedUsers = IO.createFromResource("test/passwd/passwdEmpty.json", User[].class);

        PasswdController passwdController = new PasswdController(IO.readClasspathResourceAsString("test/passwd/passwd3"));

        User userFilter = IO.createFromResource("test/passwd/userEmptyResult.json", User.class);
        List<User> actualResult = passwdController.getUsers(userFilter);
        ReflectionAssert.assertReflectionEquals(Arrays.asList(expectedUsers), actualResult, LENIENT_ORDER);
    }

    @Test
    public void testGetUserByUid() {
        User expectedUser = IO.createFromResource("test/passwd/userbyuid.json", User.class);
        PasswdController passwdController = new PasswdController(IO.readClasspathResourceAsString("test/passwd/passwd3"));
        User userFilter = IO.createFromResource("test/passwd/userbyuid.json", User.class);
        User actualResult = passwdController.getUserByUid(userFilter.getUid());
        ReflectionAssert.assertReflectionEquals(expectedUser, actualResult);
    }

    @Test
    public void testGetUserByUidBad() {
        Integer BAD_UID = 666;
        PasswdController passwdController = new PasswdController(IO.readClasspathResourceAsString("test/passwd/passwd3"));

        User actualResult = passwdController.getUserByUid(BAD_UID);
        Assert.assertNull(actualResult);
    }

    @Test(expectedExceptions = RuntimeException.class, expectedExceptionsMessageRegExp = "MalForm passwd file")
    public void testBadPasswd() {
        PasswdController passwdController = new PasswdController(IO.readClasspathResourceAsString("test/passwd/passwdBad"));
        passwdController.getUsers();
    }

    @Test
    public void testEmptyPasswd() {
        PasswdController passwdController = new PasswdController("");
        List<User> actualResult = passwdController.getUsers();
        Assert.assertTrue(actualResult.isEmpty());
    }

}
