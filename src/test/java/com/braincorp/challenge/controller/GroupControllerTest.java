package com.braincorp.challenge.controller;

import com.braincorp.challenge.model.Group;
import com.braincorp.challenge.model.User;
import com.braincorp.challenge.util.IO;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import java.util.Arrays;
import java.util.List;

import static org.unitils.reflectionassert.ReflectionComparatorMode.LENIENT_ORDER;

public class GroupControllerTest {
    @Test
    public void testGetGroups() {
        Group[] expectedGroups = IO.createFromResource("test/group/group3.json", Group[].class);
        GroupController groupController = new GroupController(IO.readClasspathResourceAsString("test/group/group3"));
        List<Group> actualResult = groupController.getGroups();
        ReflectionAssert.assertReflectionEquals(Arrays.asList(expectedGroups), actualResult, LENIENT_ORDER);
    }

    @Test
    public void testGetGroupsByUsername() {
        User expectedUser = IO.createFromResource("test/passwd/usergroup.json", User.class);
        Group[] expectedGroups = IO.createFromResource("test/group/group2.json", Group[].class);
        GroupController groupController = new GroupController(IO.readClasspathResourceAsString("test/group/groupby"));

        List<Group> actualResult = groupController.getGroupsByUsername(expectedUser.getName());

        ReflectionAssert.assertReflectionEquals(expectedGroups, actualResult, LENIENT_ORDER);
    }

    @Test
    public void testGetQueryGroupsbyusername() throws IllegalAccessException {
        Group[] expectedGroups = IO.createFromResource("test/group/group1.json", Group[].class);
        GroupController groupController = new GroupController(IO.readClasspathResourceAsString("test/group/groupby"));
        Group groupFilter = IO.createFromResource("test/group/groupfiltername.json", Group.class);

        List<Group> actualResult = groupController.getGroups(groupFilter);
        ReflectionAssert.assertReflectionEquals(Arrays.asList(expectedGroups), actualResult, LENIENT_ORDER);
    }

    @Test
    public void testGetQueryGroupsByGid() throws IllegalAccessException {
        Group[] expectedGroups = IO.createFromResource("test/group/group1.json", Group[].class);
        GroupController groupController = new GroupController(IO.readClasspathResourceAsString("test/group/groupby"));
        Group groupFilter = IO.createFromResource("test/group/groupfiltergid.json", Group.class);

        List<Group> actualResult = groupController.getGroups(groupFilter);
        ReflectionAssert.assertReflectionEquals(Arrays.asList(expectedGroups), actualResult, LENIENT_ORDER);
    }

    @Test
    public void testGetQueryGroupsNull() throws IllegalAccessException {
        Group[] expectedGroups = IO.createFromResource("test/group/group3.json", Group[].class);
        GroupController groupController = new GroupController(IO.readClasspathResourceAsString("test/group/group3"));
        Group groupFilter = IO.createFromResource("test/group/groupfilternull.json", Group.class);
        List<Group> actualResult = groupController.getGroups(groupFilter);
        ReflectionAssert.assertReflectionEquals(Arrays.asList(expectedGroups), actualResult, LENIENT_ORDER);
    }

    @Test
    public void testGetQueryGroupsMembers() throws IllegalAccessException {
        Group[] expectedGroups = IO.createFromResource("test/group/group2.json", Group[].class);
        GroupController groupController = new GroupController(IO.readClasspathResourceAsString("test/group/groupby"));
        Group groupFilter = IO.createFromResource("test/group/groupfiltermembers.json", Group.class);
        List<Group> actualResult = groupController.getGroups(groupFilter);
        ReflectionAssert.assertReflectionEquals(Arrays.asList(expectedGroups), actualResult, LENIENT_ORDER);
    }

    @Test
    public void testGetQueryGroupsEmptyMembers() throws IllegalAccessException {
        Group[] expectedGroups = IO.createFromResource("test/group/group3.json", Group[].class);
        GroupController groupController = new GroupController(IO.readClasspathResourceAsString("test/group/group3"));
        Group groupFilter = IO.createFromResource("test/group/groupfilternomember.json", Group.class);
        List<Group> actualResult = groupController.getGroups(groupFilter);
        ReflectionAssert.assertReflectionEquals(Arrays.asList(expectedGroups), actualResult, LENIENT_ORDER);
    }

    @Test
    public void testGetGroupByGid() {
        Group expectedGroup = IO.createFromResource("test/group/group.json", Group.class);
        GroupController groupController = new GroupController(IO.readClasspathResourceAsString("test/group/groupby"));
        Group actualResult = groupController.getGroupByGid(expectedGroup.getGid());
        ReflectionAssert.assertReflectionEquals(expectedGroup, actualResult, LENIENT_ORDER);
    }

    @Test
    public void testGetGroupByGidBad() {
        Integer BAD_GID = 666;
        GroupController groupController = new GroupController(IO.readClasspathResourceAsString("test/group/groupby"));

        Group actualResult = groupController.getGroupByGid(BAD_GID);
        Assert.assertNull(actualResult);
    }

    @Test(expectedExceptions = RuntimeException.class, expectedExceptionsMessageRegExp = "MalForm group file")
    public void testBadGroup() {
        GroupController groupController = new GroupController(IO.readClasspathResourceAsString("test/group/groupBad"));
        groupController.getGroups();
    }

    @Test(expectedExceptions = RuntimeException.class, expectedExceptionsMessageRegExp = "MalForm group file")
    public void testBadGroup2() {
        GroupController groupController = new GroupController(IO.readClasspathResourceAsString("test/group/groupBad2"));
        groupController.getGroups();
    }

}
