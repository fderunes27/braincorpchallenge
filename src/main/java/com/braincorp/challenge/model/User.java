package com.braincorp.challenge.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.lang.reflect.Field;

@JsonIgnoreProperties
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "User", description = "The User model")
public class User {
    @ApiModelProperty
    private String name;

    @ApiModelProperty
    private Integer uid;

    @ApiModelProperty
    private Integer gid;

    @ApiModelProperty
    private String comment;

    @ApiModelProperty
    private String home;

    @ApiModelProperty
    private String shell;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getShell() {
        return shell;
    }

    public void setShell(String shell) {
        this.shell = shell;
    }

    @JsonIgnore
    public boolean isMatching(User userCriteria) throws IllegalAccessException {
        if (userCriteria == null || userCriteria.checkNull()) {
            return true;
        }

        boolean isMatching;
        isMatching = (userCriteria.getName() != null) ? userCriteria.getName().equals(this.getName()) : true;
        isMatching = (isMatching && userCriteria.getUid() != null) ? userCriteria.getUid().equals(this.getUid()) : isMatching;
        isMatching = (isMatching && userCriteria.getGid() != null) ? userCriteria.getGid().equals(this.getGid()) : isMatching;
        isMatching = (isMatching && userCriteria.getComment() != null) ? userCriteria.getComment().equals(this.getComment()) : isMatching;
        isMatching = (isMatching && userCriteria.getHome() != null) ? userCriteria.getHome().equals(this.getHome()) : isMatching;
        isMatching = (isMatching && userCriteria.getShell() != null) ? userCriteria.getShell().equals(this.getShell()) : isMatching;

        return isMatching;
    }

    @JsonIgnore
    private boolean checkNull() throws IllegalAccessException {
        for (Field f : getClass().getDeclaredFields())
            if (f.get(this) != null)
                return false;
        return true;
    }

}
