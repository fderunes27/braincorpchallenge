package com.braincorp.challenge.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.lang.reflect.Field;
import java.util.List;

@JsonIgnoreProperties
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "Group", description = "The Group model")
public class Group {
    @ApiModelProperty
    private String name;

    @ApiModelProperty
    private Integer gid;

    @ApiModelProperty
    private List<String> members;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    @JsonIgnore
    public boolean isMatching(Group groupCriteria) throws IllegalAccessException {
        if (groupCriteria == null || groupCriteria.checkNull()) {
            return true;
        }
        boolean isMatching;
        isMatching = (groupCriteria.getName() != null) ? groupCriteria.getName().equals(this.getName()) : true;
        isMatching = (isMatching && groupCriteria.getGid() != null) ? groupCriteria.getGid().equals(this.getGid()) : isMatching;


        if (isMatching && groupCriteria.getMembers() != null) {
             if (groupCriteria.getMembers().isEmpty()) {
                return true;
            }
            for (String groupMember : groupCriteria.getMembers()) {
                if (!this.getMembers().contains(groupMember)) {
                    return false;
                }
            }
        }


        return isMatching;
    }

    @JsonIgnore
    private boolean checkNull() throws IllegalAccessException {
        for (Field f : getClass().getDeclaredFields())
            if (f.get(this) != null)
                return false;
        return true;
    }
}
