package com.braincorp.challenge.service;

import com.braincorp.challenge.controller.GroupController;
import com.braincorp.challenge.controller.PasswdController;
import com.braincorp.challenge.model.Error;
import com.braincorp.challenge.model.Group;
import com.braincorp.challenge.model.User;
import com.google.common.annotations.VisibleForTesting;
import io.swagger.annotations.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


@Path("/")
@Api(value = "Braincorp Challenge Service:", description = "Braincorp Challenge Service operations")
public class PasswdService {
    private static Logger LOG = LogManager.getLogger(PasswdService.class);
    private PasswdController passwdController;

    private GroupController groupController;

    public List<Error> getServiceErrors() {
        return serviceErrors;
    }

    private List<Error> serviceErrors = new ArrayList<>();

    public PasswdService() {
        try {
            try {
                if (System.getProperty("braincorp.challenge.passwd.path") != null) {
                    this.passwdController = new PasswdController(new String(Files.readAllBytes(Paths.get(System.getProperty("braincorp.challenge.passwd.path")))));
                } else {
                    this.passwdController = new PasswdController(new String(Files.readAllBytes(Paths.get("/etc/passwd"))));
                }
            } catch (IOException ioe) {
                LOG.error(ioe, ioe);
                serviceErrors.add(new Error(500, "Unable to read passwd file"));
            }

            try {
                if (System.getProperty("braincorp.challenge.group.path") != null) {
                    this.groupController = new GroupController(new String(Files.readAllBytes(Paths.get(System.getProperty("braincorp.challenge.group.path")))));
                } else {
                    this.groupController = new GroupController(new String(Files.readAllBytes(Paths.get("/etc/groups"))));
                }
            } catch (IOException ioe) {
                LOG.error(ioe, ioe);
                serviceErrors.add(new Error(500, "Unable to read group file"));
            }
        } catch (Exception e) {
            LOG.error(e, e);
            serviceErrors.add(new Error(500, "Unknown Server Error, contact Server Admin"));
        }
    }


    @VisibleForTesting
    public PasswdService(PasswdController passwdController, GroupController groupController, List<Error> serviceErrors) {
        this.passwdController = passwdController;
        this.groupController = groupController;
        this.serviceErrors = serviceErrors;
    }

    @GET
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get the list of users", response = User.class, responseContainer = "List", notes = "Get the list of users")
    @ApiResponses(value = {
            @ApiResponse(code = HttpURLConnection.HTTP_OK, response = User.class, message = "Returns the list of users"),
            @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, response = Error.class, message = "Internal server problems")})
    public Response getUsers() {
        try {
            if (serviceErrors.isEmpty()) {
                return Response.ok(passwdController.getUsers()).build();
            } else {
                LOG.error("error instantiating the Service");
                return Response.serverError().entity(serviceErrors).build();
            }
        } catch (RuntimeException re) {
            LOG.error(re, re);
            serviceErrors.add(new Error(500, re.getMessage()));
            return Response.serverError().entity(serviceErrors).build();
        } catch (Exception e) {
            LOG.error(e, e);
            serviceErrors.add(new Error(500, "Unknown Server Error, contact Server Admin"));
            return Response.serverError().entity(serviceErrors).build();
        }
    }

    @GET
    @Path("/users/query")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get the list of users matching all the specified query fields", response = User.class, responseContainer = "List", notes = "Get the list of users matching all the specified query fields")
    @ApiResponses(value = {
            @ApiResponse(code = HttpURLConnection.HTTP_OK, response = User.class, message = "Returns the list of users"),
            @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, response = Error.class, message = "Internal server problems")})
    public Response getUsersByQuery(
            @ApiParam(value = "the user name", required = false) @QueryParam("name") String name,
            @ApiParam(value = "the user uid", required = false) @QueryParam("uid") Integer uid,
            @ApiParam(value = "the user gid", required = false) @QueryParam("gid") Integer gid,
            @ApiParam(value = "the user comment", required = false) @QueryParam("comment") String comment,
            @ApiParam(value = "the user home", required = false) @QueryParam("home") String home,
            @ApiParam(value = "the user shell", required = false) @QueryParam("shell") String shell
    ) {
        try {
            if (serviceErrors.isEmpty()) {
                User userFilter = new User();
                userFilter.setName(name);
                userFilter.setUid(uid);
                userFilter.setGid(gid);
                userFilter.setComment(comment);
                userFilter.setHome(home);
                userFilter.setShell(shell);

                return Response.ok(passwdController.getUsers(userFilter)).build();
            } else {
                LOG.error("error instantiating the Service");
                return Response.serverError().entity(serviceErrors).build();
            }
        } catch (RuntimeException re) {
            LOG.error(re, re);
            serviceErrors.add(new Error(500, re.getMessage()));
            return Response.serverError().entity(serviceErrors).build();
        } catch (Exception e) {
            LOG.error(e, e);
            serviceErrors.add(new Error(500, "Unknown Server Error, contact Server Admin"));
            return Response.serverError().entity(serviceErrors).build();
        }
    }

    @GET
    @Path("/user/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get the user for a given uid", response = User.class, notes = "Get the user for a given uid")
    @ApiResponses(value = {
            @ApiResponse(code = HttpURLConnection.HTTP_OK, response = User.class, message = "Returns the list of users"),
            @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, response = Error.class, message = "No user found"),
            @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, response = Error.class, message = "Internal server problems")})
    public Response getUserByUid(
            @ApiParam(value = "the user uid", required = true) @PathParam("uid") Integer uid
    ) {
        try {
            if (serviceErrors.isEmpty()) {
                User user = passwdController.getUserByUid(uid);
                if (user == null) {
                    serviceErrors.add(new Error(404, "No user information found for the uid " + uid));
                    return Response.status(404).entity(serviceErrors).build();
                } else {
                    LOG.error("error instantiating the Service");
                    return Response.ok(user).build();
                }
            } else {
                return Response.serverError().entity(serviceErrors).build();
            }
        } catch (RuntimeException re) {
            LOG.error(re, re);
            serviceErrors.add(new Error(500, re.getMessage()));
            return Response.serverError().entity(serviceErrors).build();
        } catch (Exception e) {
            LOG.error(e, e);
            serviceErrors.add(new Error(500, "Unknown Server Error, contact Server Admin"));
            return Response.serverError().entity(serviceErrors).build();
        }
    }

    @GET
    @Path("/users/{uid}/groups")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get the list of groups for the given uid", response = Group.class, responseContainer = "List", notes = "Get the list of the given uid")
    @ApiResponses(value = {
            @ApiResponse(code = HttpURLConnection.HTTP_OK, response = Group.class, message = "Returns the list of groups for the given uid"),
            @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, response = Error.class, message = "Internal server problems")})
    public Response getGroupsByUid(
            @ApiParam(value = "the user uid", required = true) @PathParam("uid") Integer uid
    ) {
        try {
            if (serviceErrors.isEmpty()) {
                String username = passwdController.getUserByUid(uid).getName();
                return Response.ok(groupController.getGroupsByUsername(username)).build();
            } else {
                LOG.error("error instantiating the Service");
                return Response.serverError().entity(serviceErrors).build();
            }
        } catch (RuntimeException re) {
            LOG.error(re, re);
            serviceErrors.add(new Error(500, re.getMessage()));
            return Response.serverError().entity(serviceErrors).build();
        } catch (Exception e) {
            LOG.error(e, e);
            serviceErrors.add(new Error(500, "Unknown Server Error, contact Server Admin"));
            return Response.serverError().entity(serviceErrors).build();
        }


    }

    @GET
    @Path("/groups")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get the list of groups", response = User.class, responseContainer = "List", notes = "Get the list of groups")
    @ApiResponses(value = {
            @ApiResponse(code = HttpURLConnection.HTTP_OK, response = User.class, message = "Returns the list of groups"),
            @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, response = Error.class, message = "Internal server problems")})
    public Response getGroups() {
        try {
            if (serviceErrors.isEmpty()) {
                return Response.ok(groupController.getGroups()).build();
            } else {
                LOG.error("error instantiating the Service");
                return Response.serverError().entity(serviceErrors).build();
            }

        } catch (RuntimeException re) {
            LOG.error(re, re);
            serviceErrors.add(new Error(500, re.getMessage()));
            return Response.serverError().entity(serviceErrors).build();
        } catch (Exception e) {
            LOG.error(e, e);
            serviceErrors.add(new Error(500, "Unknown Server Error, contact Server Admin"));
            return Response.serverError().entity(serviceErrors).build();
        }
    }

    @GET
    @Path("/groups/query")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get the list of groups matching all the specified query fields", response = Group.class, responseContainer = "List", notes = "Get the list of groups matching all the specified query fields")
    @ApiResponses(value = {
            @ApiResponse(code = HttpURLConnection.HTTP_OK, response = User.class, message = "Returns the list of groups"),
            @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, response = Error.class, message = "Internal server problems")})
    public Response getGroupsByQuery(
            @ApiParam(value = "the group name", required = false) @QueryParam("name") String name,
            @ApiParam(value = "the user gid", required = false) @QueryParam("gid") Integer gid,
            @ApiParam(value = "the members", required = false) @QueryParam("member") List<String> members
    ) {
        try {
            if (serviceErrors.isEmpty()) {
                Group groupFilter = new Group();
                groupFilter.setName(name);
                groupFilter.setGid(gid);
                groupFilter.setMembers(members);

                return Response.ok(groupController.getGroups(groupFilter)).build();
            } else {
                LOG.error("error instantiating the Service");
                return Response.serverError().entity(serviceErrors).build();
            }

        } catch (RuntimeException re) {
            LOG.error(re, re);
            serviceErrors.add(new Error(500, re.getMessage()));
            return Response.serverError().entity(serviceErrors).build();
        } catch (Exception e) {
            LOG.error(e, e);
            serviceErrors.add(new Error(500, "Unknown Server Error, contact Server Admin"));
            return Response.serverError().entity(serviceErrors).build();
        }
    }

    @GET
    @Path("/group/{gid}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get the group by gid", response = Group.class, responseContainer = "List", notes = "Get the group by gid")
    @ApiResponses(value = {
            @ApiResponse(code = HttpURLConnection.HTTP_OK, response = User.class, message = "Returns the group"),
            @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, response = Error.class, message = "No group found"),
            @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, response = Error.class, message = "Internal server problems")})
    public Response getGroupsByGid(
            @ApiParam(value = "the group gid", required = true) @PathParam("gid") Integer gid
    ) {
        try {
            if (serviceErrors.isEmpty()) {
                Group group = groupController.getGroupByGid(gid);
                if (group == null) {
                    LOG.error("404, \"No group information found for the gid " + gid + "\")");
                    serviceErrors.add(new Error(404, "No group information found for the gid " + gid));
                    return Response.status(404).entity(serviceErrors).build();
                } else {
                    return Response.ok(group).build();
                }
            } else {
                LOG.error("error instantiating the Service");
                return Response.serverError().entity(serviceErrors).build();
            }
        } catch (RuntimeException re) {
            LOG.error(re, re);
            serviceErrors.add(new Error(500, re.getMessage()));
            return Response.serverError().entity(serviceErrors).build();
        } catch (Exception e) {
            LOG.error(e, e);
            serviceErrors.add(new Error(500, "Unknown Server Error, contact Server Admin"));
            return Response.serverError().entity(serviceErrors).build();
        }
    }
}
