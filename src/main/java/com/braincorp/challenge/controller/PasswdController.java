package com.braincorp.challenge.controller;

import com.braincorp.challenge.model.User;
import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is controlling the operations of reading the passwd content and returning its infomration
 */
public class PasswdController extends baseController {
    private String passwdContent;

    public String getPasswdContent() {
        return passwdContent;
    }

    public PasswdController(String passwdContent) {
        this.passwdContent = passwdContent;
    }

    /**
     * @return a list of all the users from the passwd content
     */
    public List<User> getUsers() {
        List<User> users = new ArrayList<>();
        String[] usersFromFile = getPasswdContent().split("\\r?\\n");
        for (String userLine : usersFromFile) {
            if (!userLine.isEmpty()) {
                User user = getUsersFromLine(getAttributes(userLine));
                users.add(user);
            }
        }
        return users;
    }

    /**
     * @param uid is the user id
     * @return the user for the given uid
     */
    public User getUserByUid(Integer uid) {
        String[] usersFromFile = getPasswdContent().split("\\r?\\n");

        for (String userLine : usersFromFile) {
            User user = getUsersFromLine(getAttributes(userLine));
            if (user.getUid().equals(uid)) {
                return user;
            }
        }
        return null;
    }

    /**
     * return a user object from the attributes of a line of the passwd content
     *
     * @param userAttributes user attributes from the passwd file line
     * @return a user
     */
    private User getUsersFromLine(String[] userAttributes) {
        if (userAttributes.length != 7) {
            throw new RuntimeException("MalForm passwd file");
        }
        User user = new User();
        user.setName(StringEscapeUtils.unescapeJava(userAttributes[0]).trim());
        user.setUid(Integer.parseInt(StringEscapeUtils.unescapeJava(userAttributes[2]).trim()));
        user.setGid(Integer.parseInt(StringEscapeUtils.unescapeJava(userAttributes[3]).trim()));
        user.setComment(StringEscapeUtils.unescapeJava(userAttributes[4]).trim());
        user.setHome(StringEscapeUtils.unescapeJava(userAttributes[5]).trim());
        user.setShell(StringEscapeUtils.unescapeJava(userAttributes[6]).trim());
        return user;
    }

    /**
     * return a user list object from the attributes of aline of the group content
     *
     * @param userFilter is a user with fields having non null value used as a filter
     * @return a list of users matching all of the specified user fields
     * @throws IllegalAccessException is thrown if the user filter cannot be checked for null
     */
    public List<User> getUsers(User userFilter) throws IllegalAccessException {
        List<User> users = this.getUsers();
        List<User> filteredUsersList = new ArrayList<>();

        for (User u : users) {
            if (u.isMatching(userFilter)) {
                filteredUsersList.add(u);
            }
        }
        return filteredUsersList;
    }


}
