package com.braincorp.challenge.controller;

import com.braincorp.challenge.model.Group;
import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class is controlling the operations of reading the group content and returning its infomration
 */
public class GroupController extends baseController {
    private String groupContent;

    public String getGroupContent() {
        return groupContent;
    }

    public GroupController(String groupContent) {
        this.groupContent = groupContent;
    }

    /**
     * @return a list of all the groups from the group content
     */
    public List<Group> getGroups() {
        List<Group> groups = new ArrayList<>();
        String[] groupsFromFile = getGroupContent().split("\\r?\\n");
        for (String groupLine : groupsFromFile) {
            if (!groupLine.isEmpty()) {
                Group group = getGroupsFromLine(getAttributes(groupLine));
                groups.add(group);
            }
        }
        return groups;
    }

    /**
     * @param username that is in the members list
     * @return a list of all the groups containing a given username
     */
    public List<Group> getGroupsByUsername(String username) {
        List<Group> groupForUsername = new ArrayList<>();
        List<Group> groups = getGroups();

        for (Group group : groups) {
            if (group.getMembers().contains(username)) {
                groupForUsername.add(group);
            }
        }
        return groupForUsername;
    }

    /**
     * @param gid is the group id
     * @return the group for the given gid
     */
    public Group getGroupByGid(Integer gid) {
        List<Group> groups = getGroups();
        for (Group group : groups) {
            if (group.getGid().equals(gid)) {
                return group;
            }
        }
        return null;
    }

    /**
     * @param groupFilter is a group with fields having non null value used as a filter
     * @return a list of groups matching all of the specified group fields
     * @throws IllegalAccessException is thrown if the group filter cannot be checked for null
     */
    public List<Group> getGroups(Group groupFilter) throws IllegalAccessException {
        List<Group> groups = this.getGroups();
        List<Group> filteredGroupsList = new ArrayList<>();

        for (Group g : groups) {
            if (g.isMatching(groupFilter)) {
                filteredGroupsList.add(g);
            }
        }
        return filteredGroupsList;
    }

    /**
     * return a group object from the attributes of aline of the group content
     *
     * @param groupAttributes parse group attributes
     * @return a group
     */
    private Group getGroupsFromLine(String[] groupAttributes) {
        if (groupAttributes.length < 3 || groupAttributes.length > 4) {
            throw new RuntimeException("MalForm group file");
        }
        Group group = new Group();
        group.setName(StringEscapeUtils.unescapeJava(groupAttributes[0]));
        group.setGid(Integer.parseInt(StringEscapeUtils.unescapeJava(groupAttributes[2])));
        if (groupAttributes.length == 4) {
            group.setMembers(Arrays.asList(StringEscapeUtils.unescapeJava(groupAttributes[3]).split(("\\s*,\\s*"))));
        } else {
            group.setMembers(new ArrayList<>());
        }
        return group;
    }


}
