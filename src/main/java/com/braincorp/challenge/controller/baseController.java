package com.braincorp.challenge.controller;

import java.util.regex.Pattern;

public class baseController {
    protected String[] getAttributes(String userLine) {
        String delim = ":";
        String regex = "(?<!\\\\)" + Pattern.quote(delim);
        return userLine.split(regex);
    }
}
