{
  "swagger" : "2.0",
  "info" : {
    "description" : "Braincorp Challenge Service",
    "version" : "v1",
    "title" : "Braincorp Challenge Service",
    "contact" : {
      "name" : "Franck Derunes",
      "email" : "fderunes@gmail.com"
    }
  },
  "host" : "localhost:8190",
  "basePath" : "/challenge/service",
  "tags" : [ {
    "name" : "Braincorp Challenge Service:",
    "description" : "Braincorp Challenge Service operations"
  } ],
  "schemes" : [ "http" ],
  "paths" : {
    "/group/{gid}" : {
      "get" : {
        "tags" : [ "Braincorp Challenge Service:" ],
        "summary" : "Get the group by gid",
        "description" : "Get the group by gid",
        "operationId" : "getGroupsByGid",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "gid",
          "in" : "path",
          "description" : "the group gid",
          "required" : true,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "Returns the group",
            "schema" : {
              "$ref" : "#/definitions/User"
            }
          },
          "404" : {
            "description" : "No group found",
            "schema" : {
              "$ref" : "#/definitions/Error"
            }
          },
          "500" : {
            "description" : "Internal server problems",
            "schema" : {
              "$ref" : "#/definitions/Error"
            }
          }
        }
      }
    },
    "/groups" : {
      "get" : {
        "tags" : [ "Braincorp Challenge Service:" ],
        "summary" : "Get the list of groups",
        "description" : "Get the list of groups",
        "operationId" : "getGroups",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "responses" : {
          "200" : {
            "description" : "Returns the list of groups",
            "schema" : {
              "$ref" : "#/definitions/User"
            }
          },
          "500" : {
            "description" : "Internal server problems",
            "schema" : {
              "$ref" : "#/definitions/Error"
            }
          }
        }
      }
    },
    "/groups/query" : {
      "get" : {
        "tags" : [ "Braincorp Challenge Service:" ],
        "summary" : "Get the list of groups matching all the specified query fields",
        "description" : "Get the list of groups matching all the specified query fields",
        "operationId" : "getGroupsByQuery",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "name",
          "in" : "query",
          "description" : "the group name",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "gid",
          "in" : "query",
          "description" : "the user gid",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        }, {
          "name" : "member",
          "in" : "query",
          "description" : "the members",
          "required" : false,
          "type" : "array",
          "items" : {
            "type" : "string"
          },
          "collectionFormat" : "multi"
        } ],
        "responses" : {
          "200" : {
            "description" : "Returns the list of groups",
            "schema" : {
              "$ref" : "#/definitions/User"
            }
          },
          "500" : {
            "description" : "Internal server problems",
            "schema" : {
              "$ref" : "#/definitions/Error"
            }
          }
        }
      }
    },
    "/user/{uid}" : {
      "get" : {
        "tags" : [ "Braincorp Challenge Service:" ],
        "summary" : "Get the list of users",
        "description" : "Get the list of users",
        "operationId" : "getUserByUid",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "uid",
          "in" : "path",
          "description" : "the user uid",
          "required" : true,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "Returns the list of users",
            "schema" : {
              "$ref" : "#/definitions/User"
            }
          },
          "404" : {
            "description" : "No user found",
            "schema" : {
              "$ref" : "#/definitions/Error"
            }
          },
          "500" : {
            "description" : "Internal server problems",
            "schema" : {
              "$ref" : "#/definitions/Error"
            }
          }
        }
      }
    },
    "/users" : {
      "get" : {
        "tags" : [ "Braincorp Challenge Service:" ],
        "summary" : "Get the list of users",
        "description" : "Get the list of users",
        "operationId" : "getUsers",
        "produces" : [ "application/json" ],
        "responses" : {
          "200" : {
            "description" : "Returns the list of users",
            "schema" : {
              "$ref" : "#/definitions/User"
            }
          },
          "500" : {
            "description" : "Internal server problems",
            "schema" : {
              "$ref" : "#/definitions/Error"
            }
          }
        }
      }
    },
    "/users/query" : {
      "get" : {
        "tags" : [ "Braincorp Challenge Service:" ],
        "summary" : "Get the list of users matching all the specified query fields",
        "description" : "Get the list of users matching all the specified query fields",
        "operationId" : "getUsersByQuery",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "name",
          "in" : "query",
          "description" : "the user name",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "uid",
          "in" : "query",
          "description" : "the user uid",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        }, {
          "name" : "gid",
          "in" : "query",
          "description" : "the user gid",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        }, {
          "name" : "comment",
          "in" : "query",
          "description" : "the user comment",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "home",
          "in" : "query",
          "description" : "the user home",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "shell",
          "in" : "query",
          "description" : "the user shell",
          "required" : false,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "Returns the list of users",
            "schema" : {
              "$ref" : "#/definitions/User"
            }
          },
          "500" : {
            "description" : "Internal server problems",
            "schema" : {
              "$ref" : "#/definitions/Error"
            }
          }
        }
      }
    },
    "/users/{uid}/groups" : {
      "get" : {
        "tags" : [ "Braincorp Challenge Service:" ],
        "summary" : "Get the list of groups for the given uid",
        "description" : "Get the list of the given uid",
        "operationId" : "getGroupsByUid",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "uid",
          "in" : "path",
          "description" : "the user uid",
          "required" : true,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "Returns the list of groups for the given uid",
            "schema" : {
              "$ref" : "#/definitions/Group"
            }
          },
          "500" : {
            "description" : "Internal server problems",
            "schema" : {
              "$ref" : "#/definitions/Error"
            }
          }
        }
      }
    }
  },
  "definitions" : {
    "Error" : {
      "type" : "object",
      "properties" : {
        "description" : {
          "type" : "string"
        },
        "code" : {
          "type" : "integer",
          "format" : "int32"
        }
      },
      "description" : "The Error model"
    },
    "Group" : {
      "type" : "object",
      "properties" : {
        "name" : {
          "type" : "string"
        },
        "gid" : {
          "type" : "integer",
          "format" : "int32"
        },
        "members" : {
          "type" : "array",
          "items" : {
            "type" : "string"
          }
        }
      },
      "description" : "The Group model"
    },
    "User" : {
      "type" : "object",
      "properties" : {
        "name" : {
          "type" : "string"
        },
        "uid" : {
          "type" : "integer",
          "format" : "int32"
        },
        "gid" : {
          "type" : "integer",
          "format" : "int32"
        },
        "comment" : {
          "type" : "string"
        },
        "home" : {
          "type" : "string"
        },
        "shell" : {
          "type" : "string"
        }
      },
      "description" : "The User model"
    }
  }
}